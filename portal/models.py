from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Q
from django.shortcuts import get_object_or_404

from .managers import PostManager
from .services import dateservice


class User(AbstractUser):
    def create(username, email, first_name, last_name, password):
        return User(
            username=username,
            email=email,
            first_name=first_name,
            last_name=last_name,
            password=password)

    def get_by_email(email):
        return get_object_or_404(User, email=email)

    def get_by_id(id):
        return get_object_or_404(User, id=id)

    def look_for(query):
        return User.objects.filter(
            Q(first_name__icontains=query) | Q(last_name__icontains=query) |
            Q(email__icontains=query))

    def __repr__(self):

        return (self.first_name + ' ' + self.email + ' ' + self.username + ' ' +
                self.password)


class Category(models.Model):
    category = models.CharField(max_length=40)

    def __repr__(self):
        return 'Category: {}'.format(self.category)


class Post(models.Model):
    title = models.CharField(max_length=1000)
    body = models.CharField(max_length=10000)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default='0000')
    categories = models.ManyToManyField(Category)
    creation_date = models.DateTimeField(auto_now_add=True, blank=True)
    objects = PostManager()

    def dd(self):
        return dateservice.get_period_from_date_to_now(self.creation_date)

    def __repr__(self):
        return self.body

    class Meta:
        ordering = ['-creation_date']


class Keyword(models.Model):
    word = models.CharField(max_length=30)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __repr__(self):
        return 'Keyword: {} for post id: {}'.format(self.word, self.post.id)


class Search(models.Model):
    occurrences = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=0)
    creation_date = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        abstract = True
        ordering = ['-occurrences']


class PostSearch(Search):
    word = models.CharField(max_length=30)

    def __repr__(self):
        return 'PostSearch: {}, occ: {} created: {}'.format(
            self.word, self.occurrences, self.creation_date)


class UserSearch(Search):
    searched_user_id = models.IntegerField()

    def __repr__(self):
        return 'UserSearch: {}, occ: {}'.format(self.user_id, self.occurrences)


class Comment(models.Model):
    body = models.CharField(max_length=10000, default='')
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    creation_date = models.DateTimeField(auto_now_add=True, blank=True)
    type = models.CharField(max_length=1, default='A')
    """
    Part of model used in case of disagree comment.
    |  |  |  |
    V  V  V  V
    """
    from collections import namedtuple
    Links = namedtuple('Links', ['linkA', 'linkB'])

    linkA = models.URLField(max_length=2000, default='')
    linkB = models.URLField(max_length=2000, default='')

    links = Links(linkA=linkA, linkB=linkB)

    def get_period_from_creation_date(self):
        return dateservice.get_period_from_date_to_now(self.creation_date)

    def __repr__(self):
        result = 'Author: {}, body: {}, date: {}, type: {}'.format(
            self.author, self.body, self.creation_date, self.type)
        return result

    class Meta:
        ordering = ['-creation_date']


class Classifier(models.Model):
    text = models.CharField(max_length=10000)
    category = models.CharField(max_length=20)

    def __repr__(self):
        return 'text: {}, cat: {}'.format(self.text, self.category)
