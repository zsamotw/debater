from django.http import Http404
from django.test import TestCase

from ..models import User

usernames = ['username1', 'username2', 'username3']
emails = ['email1', 'email2', 'email3']
first_names = ['fn1', 'fn2', 'fn3']
last_names = ['ln1', 'ln2', 'ln3']
passwords = ['pass1', 'pass2', 'pass3']

wrong_email = 'not@existing.email'
wrong_id = 100


class TestUser(TestCase):
    def setUp(self):
        """
        Setting database: create three users with
        list comprehension.
        """
        self.users_from_raw_api = [
            User.objects.create_user(
                username=un, email=e, first_name=fn, last_name=ln, password=p)
            for (un, e, fn, ln, p) in zip(usernames, emails, first_names,
                                          last_names, passwords)
        ]
        for user in self.users_from_raw_api:
            user.save()

    def test_get_by_email(self):
        index = 1
        user = User.get_by_email(emails[index])
        self.assertEqual(self.users_from_raw_api[index], user)

    def test_get_by_email_with_failure(self):
        with self.assertRaises(Http404):
            User.get_by_email(wrong_email)

    def test_get_by_id(self):
        id = 1
        index = 0
        user = User.get_by_id(id)
        self.assertEqual(self.users_from_raw_api[index], user)

    def test_get_by_id_with_failure(self):
        with self.assertRaises(Http404):
            User.get_by_id(wrong_id)

    def test_look_for_first_with_name(self):
        query = 'fn1'
        users = User.look_for(query)
        self.assertEqual(len(users), 1)

    def test_look_for_last_with_name(self):
        query = 'ln1'
        users = User.look_for(query)
        self.assertEqual(len(users), 1)

    def test_look_for_with_email(self):
        query = 'email1'
        users = User.look_for(query)
        self.assertEqual(len(users), 1)

    def test_look_for_with_query_fit_to_all_objects(self):
        query = 'email'
        users = User.look_for(query)
        self.assertEqual(len(users), 3)

    def test_look_for_with_query_fit_to_no_objects(self):
        query = wrong_email
        users = User.look_for(query)
        self.assertEqual(len(users), 0)
