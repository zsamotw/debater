from django.test import TestCase
from portal.services.beautifulsoupservice import (
    get_how_many_keywords_in_soup, get_soup, is_keyword_in_soup)

from ..models import Keyword

content = """
<body>
This is a body of web siteor where many soups are in the soups.
</body>
"""

keyword = Keyword('web')
keywordA = Keyword('none')
keywords = [Keyword('SitE')]
keywordsA = [Keyword('site'), Keyword('soUp'), Keyword('wood')]
keywordsC = [Keyword('dog')]

soup = get_soup(content)


class TestBeautyfulsoup(TestCase):
    def test_is_keyword_in_soup_for_true(self):
        result = is_keyword_in_soup(keyword, soup)
        assert (result is True)

    # def test_is_keyword_in_soup_for_false(self):
    #     result = is_keyword_in_soup(keywordA, soup)
    #     assert (result is False)

    # def test_is_keyword_in_soup_for_empty_keyword(self):
    #     result = is_keyword_in_soup(Keyword(''), soup)
    #     assert (result is False)

    # def test_is_keyword_in_soup_for_spaces(self):
    #     result = is_keyword_in_soup(Keyword(' '), soup)
    #     assert (result is False)

    # def test_how_many_keywords_in_soup_for_one(self):
    #     result = get_how_many_keywords_in_soup(keywords, soup)
    #     assert (result == 1)

    # def test_how_many_keywords_in_soup_for_two(self):
    #     result = get_how_many_keywords_in_soup(keywordsA, soup)
    #     assert (result == 2)

    # def test_how_many_keywords_in_soup_for_zero(self):
    #     result = get_how_many_keywords_in_soup(keywordsC, soup)
    #     assert (result == 0)

    def test_how_many_keywords_in_soup_for_empty_keywords(self):
        result = get_how_many_keywords_in_soup([], soup)
        assert (result == 0)
