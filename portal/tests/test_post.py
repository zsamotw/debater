from django.test import TestCase
from django.utils import timezone

from ..models import Post, User
from ..services import (keywordservice, postsearchservice, postservice,
                        usersearchservice)


class TestPost(TestCase):
    def setUp(self):
        """
        Setting database: create three users with
        list comprehension and three posts with it's content.
        """

        usernames = ['username1', 'username2', 'username3']
        emails = ['email1', 'email2', 'email3']
        first_names = ['fn1', 'fn2', 'fn3']
        last_names = ['ln1', 'ln2', 'ln3']
        passwords = ['pass1', 'pass2', 'pass3']

        titles = ['title1', 'title2', 'title3']
        bodies = ['body1', 'body2', 'body3']

        self.users_from_raw_api = [
            User.objects.create_user(
                username=un, email=e, first_name=fn, last_name=ln, password=p)
            for (un, e, fn, ln, p) in
            zip(usernames, emails, first_names, last_names,
                passwords)
        ]
        for user in self.users_from_raw_api:
            user.save()

        self.posts_from_raw_api = [
            Post.objects.create(
                title=t, body=b, author=u, creation_date=timezone.now()) for
            (t, b, u) in zip(titles, bodies, self.users_from_raw_api)
        ]
        for post in self.posts_from_raw_api:
            post.save()

    def test_get_all(self):
        posts = Post.objects.all()
        self.assertEqual(len(posts), 3)

    def test_get_user_posts_for_existing_user(self):
        user_id = 1
        posts = Post.objects.get_author_posts(user_id)
        self.assertEqual(len(posts), 1)

    def test_get_user_posts_for_notexisting_user(self):
        user_id = 10
        posts = Post.objects.get_author_posts(user_id)
        self.assertEqual(len(posts), 0)

    def test_look_for_title(self):
        query = 'title1'
        posts = Post.objects.look_for(query)
        self.assertEqual(len(posts), 1)

    def test_look_for_body(self):
        query = 'body1'
        posts = Post.objects.look_for(query)
        self.assertEqual(len(posts), 1)

    def test_look_for_more_general_query(self):
        query = 'body'
        posts = Post.objects.look_for(query)
        self.assertEqual(len(posts), 3)


class TestPostForUserPrefs(TestCase):

    def setUp(self):
        """
        Setting database: create three users with
        list comprehension and three posts with it's content.
        """
        usernames = ['username1', 'username2', 'username3']
        emails = ['email1', 'email2', 'email3']
        first_names = ['fn1', 'fn2', 'fn3']
        last_names = ['ln1', 'ln2', 'ln3']
        passwords = ['pass1', 'pass2', 'pass3']

        titles = ['title1', 'title2', 'title3']
        bodies = ['woOd and woLf', 'fooD and Dog', 'hoMe aNd hoMe']

        self.users_from_raw_api = [
            User.objects.create_user(
                username=un, email=e, first_name=fn, last_name=ln, password=p)
            for (un, e, fn, ln, p) in zip(usernames, emails, first_names,
                                          last_names, passwords)
        ]
        for user in self.users_from_raw_api:
            user.save()

        self.posts_from_raw_api = [
            Post.objects.create(
                title=t, body=b, author=u, creation_date=timezone.now())
            for (t, b, u) in zip(titles, bodies, self.users_from_raw_api)
        ]
        for post in self.posts_from_raw_api:
            keywordservice.create_from_post(post)
            post.save()

    def test_get_all(self):
        posts = Post.objects.all()
        self.assertEqual(len(posts), 3)

    def test_get_posts_with_user_search(self):
        """
        Setting database: user searches for user1:
        he looking for user2
        """
        user = self.users_from_raw_api[0]
        usersearchservice.update(2, user)
        posts = postservice.get_for_user_prefs(user, 3, 3, 3)
        self.assertEqual(posts[0].body.lower(), 'food and dog')

    def test_get_posts_with_post_search(self):
        """
        Setting database: post searches for user1:
        he looking for 'homE'
        """
        user = self.users_from_raw_api[0]
        postsearchservice.update('homE', user)
        posts = postservice.get_for_user_prefs(user, 3, 3, 3)
        self.assertEqual(posts[0].body.lower(), 'home and home')

    def test_get_posts_with_post_search_and_user_search(self):
        """
        Setting database: post searches for user1:
        he looking for 'WooD' and user2
        """
        user = self.users_from_raw_api[0]
        postsearchservice.update('WooD', user)
        usersearchservice.update(2, user)
        posts = postservice.get_for_user_prefs(user, 3, 3, 3)
        self.assertEqual(len(posts), 2)
