from django.test import TestCase
from portal.models import Keyword, Post, User
from portal.services import keywordservice


class TestKeyword(TestCase):
    def setUp(self):
        user = User.objects.create(
            username='someusername',
            email='email',
            first_name='name',
            last_name='last',
            password='secret')
        Post.objects.create(id=1, title='Post', body='body', author=user)
        Post.objects.create(
            id=2,
            title='Post-1',
            body='I was in school with my friend',
            author=user)

    def test_create_word(self):
        post = Post.objects.get(id=1)
        word = keywordservice.create('wolf', post)
        assert (len(Keyword.objects.all()) == 1)

    def test_create_from_post(self):
        post = Post.objects.get(id=2)
        keywordservice.create_from_post(post)
        assert (len(Keyword.objects.filter(post=post)) == 2)

    def test_get_row_words_for_post(self):
        post = Post.objects.get(id=2)
        keywordservice.create_from_post(post)
        keywords = keywordservice.get_row_words_for_post(post)
        assert (keywords == {'friend', 'school'})
