import factory
from portal.models import Category, Post, User


class UserFactory(factory.Factory):
    class Meta:
        model = User

    username = 'username'
    email = 'email'
    first_name = 'first'
    last_name = 'last'
    password = 'secrtet'


class CategoryFactory(factory.Factory):
    class Meta:
        model = Category


class PostFactory(factory.Factory):
    class Meta:
        model = Post

    title = 'title'
    author = UserFactory.create()
