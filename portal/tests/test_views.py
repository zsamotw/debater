from django.urls import reverse
from django.test import TestCase, Client
from .test_utilities import (add_user_to_db, login_client_user,
                             logout_client_user)


class TestViews(TestCase):
    def setUp(self):
        self.client = Client(enforce_csrf_check=True)
        add_user_to_db()

    def test_index(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'DEBATER')

    def test_mainboard(self):
        login_client_user(self)
        response = self.client.get(reverse('mainboard', args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Post')
