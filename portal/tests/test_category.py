from django.test import TestCase
from portal.services import categoryservice
from portal.models import Category

from .factories import CategoryFactory


class TestCategory(TestCase):
    def setUp(self):
        # CategoryFactory.create(category='animals')
        Category.objects.create(category='animals')

    def test_get_from_string_01(self):
        result = categoryservice.get_categories_names_from_string(
            'dog, animal fruit a.git')
        assert (result == ['dog', 'animal', 'fruit', 'a', 'git'])

    def test_get_or_create_category_animals(self):
        categories = categoryservice.get_or_create_category('animals')
        result = Category.objects.all()
        assert (len(result) == 1)

    def test_get_or_create_category_fruits(self):
        categories = categoryservice.get_or_create_category('fruits')
        result = Category.objects.all()
        assert (len(result) == 2)
