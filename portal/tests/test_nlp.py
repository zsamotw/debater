from django.test import TestCase
from portal.services import nlpservice
from textblob import Word
from .factories import PostFactory, CategoryFactory

query = 'I was in jungle with my dog Frank.'


class TestTextBlob(TestCase):
    """testing natural language processing service"""

    # def setup(self):
    #     query = 'I was in jungle with my dog Frank.'

    def test_get_text_blob_tags(self):
        tags = nlpservice.text_blob_tags(query)
        assert (len(tags) == 8)

    def test_lemmatize_jungles(self):
        word = Word('jungles')
        result = nlpservice.lemmatize(word)
        assert (result == 'jungle')

    def test_lemmatize_boys(self):
        word = Word('boys')
        result = nlpservice.lemmatize(word)
        assert (result == 'boy')

    def test_get_words_with_grammar_criterium_nouns(self):
        result = nlpservice.get_words_with_grammar_criterium(
            query, nlpservice.NOUN_PATTERN)
        assert (result == ['jungle', 'dog', 'frank'])


class TestClassifier(TestCase):
    def setUp(self):
        postA = PostFactory.create(body='I like animals')
        postB = PostFactory.create(body='Dogs are smart')
        postC = PostFactory.create(body='Cats live in factory')
        category = CategoryFactory.create(category='animals')
        for post in [postA, postB, postC]:
            nlpservice.classify_post_with_categories_and_save(post, [category])

    def test_classify_post_with_categories(self):
        post = PostFactory.create(body='I like music')
        categories = [
            CategoryFactory.create(category='music'),
            CategoryFactory.create(category='art')
        ]
        result = nlpservice.classify_post_with_categories(post, categories)
        assert (len(list(result)) == 2)

    def test_classify_text_with_cat(self):
        text = 'I want to have cat'
        cl = nlpservice.run_classifier()
        result = nlpservice.classify_text(text, cl)
        assert(result == 'animals')

    def test_classify_text_with_parrot(self):
        text = 'I want to have parrot'
        cl = nlpservice.run_classifier()
        result = nlpservice.classify_text(text, cl)
        assert(result == 'animals')
