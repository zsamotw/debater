from django.test import TestCase
from ..models import User, UserSearch
from ..services import usersearchservice

user_data = ['email', 'name', 'surname', 'somepassword']
users_ids = [3, 4, 5]


class TestSearchUser(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username=user_data[0],
            email=user_data[0],
            first_name=user_data[1],
            last_name=user_data[2],
            password=user_data[3])

        self.user_searches = [
            UserSearch(user=self.user, occurrences=occ, searched_user_id=id)
            for (occ, id) in zip(list(range(1, 4)), users_ids)
        ]
        for search in self.user_searches:
            search.save()

    def test_get_all(self):
        searches = UserSearch.objects.all()
        self.assertEqual(len(searches), 3)

    def test_update_for_id_searched_prevously(self):
        search = usersearchservice.update(3, self.user)
        self.assertEqual(search.occurrences, 2)

    def test_update_for_id_searched_first_time(self):
        notexisting_id = 11
        search = usersearchservice.update(notexisting_id, self.user)
        self.assertEqual(search.occurrences, 1)

    def test_get_searched_users_ids_for_user(self):
        users_ids = usersearchservice.get_searched_users_ids(self.user)
        self.assertEqual(len(list(users_ids)), 3)

    def test_get_searched_users_ids_for_user_with_no_searches(self):
        some_data = 'xyz'
        self.user = User(some_data, some_data, some_data, some_data)
        users_ids = usersearchservice.get_searched_users_ids(self.user)
        self.assertEqual(len(list(users_ids)), 0)
