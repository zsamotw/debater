from django.test import TestCase
from ..models import User, PostSearch
from ..services import postsearchservice

user_data = ['email', 'name', 'surname', 'somepassword']
words = ['home', 'welcome', 'amigo', 'sierra']


class TestSearchWord(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username=user_data[0],
            email=user_data[0],
            first_name=user_data[1],
            last_name=user_data[2],
            password=user_data[3])

        self.user_searches = [
            PostSearch(user=self.user, occurrences=occ, word=ww)
            for (occ, ww) in zip(list(range(1, 5)), words)
        ]
        for search in self.user_searches:
            search.save()

    def test_get_all(self):
        searches = PostSearch.objects.all()
        print(searches)
        self.assertEqual(len(searches), 4)

    def test_update_for_word_searched_previously(self):
        word = 'home'
        search = postsearchservice.update(word, self.user)
        self.assertEqual(search.occurrences, 2)

    def test_update_for_word_not_searched_previously(self):
        word = 'bear'
        search = postsearchservice.update(word, self.user)
        print(search)
        print(PostSearch.objects.all())
        self.assertEqual(search.occurrences, 1)

    def test_get_get_searched_word_for_user(self):
        words = postsearchservice.get_searched_keywords(self.user)
        self.assertEqual(len(list(words)), 4)
