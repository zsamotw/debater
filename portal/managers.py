from django.contrib.auth.models import UserManager
from django.db import models
from django.db.models import Q

from .decorators import check_object_existing


class AppUserManager(UserManager):
    def create_user(self, username, email, first_name, last_name, password):
        if not (username and email and first_name and last_name and password):
            raise ValueError('Fill form with additional data')
        user = self.model(username, email, first_name, last_name, password)
        user.save()
        return user


class PostManager(models.Manager):
    @check_object_existing
    def get_by_id(self, post_id):
        return self.get(id=post_id)

    def get_author_posts(self, user_id):
        return self.filter(author__id=user_id)

    def look_for(self, query):
        return self.filter(
            Q(title__icontains=query) | Q(body__icontains=query))
