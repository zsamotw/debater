from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404


def check_object_existing(func):
    def checker(*args, **kwargs):
        result = None
        try:
            result = func(*args, **kwargs)
        except ObjectDoesNotExist:
            raise Http404('ObjectDoesNotExist')
        else:
            return result
