from django import forms
from portal.services import beautifulsoupservice


class LoginForm(forms.Form):
    user_email = forms.EmailField(label='Your email', max_length=20)
    user_password = forms.CharField(
        label='Your password', max_length=20, widget=forms.PasswordInput())


class UserCreationForm(forms.Form):
    email = forms.EmailField(label='Your email', max_length=20)
    first_name = forms.CharField(label='Your name', max_length=20)
    last_name = forms.CharField(label='Your surname', max_length=20)
    password = forms.CharField(
        label='Your password', max_length=20, widget=forms.PasswordInput())


class PostCreationForm(forms.Form):
    title = forms.CharField(
        label='title', initial='Title of your post', max_length=2000)
    body = forms.CharField(
        label='body', initial='Write what you think ?', widget=forms.Textarea)

    categories = forms.CharField(
        label='categories', initial='Add categories', max_length=100)


class CommentCreationForm(forms.Form):
    body = forms.CharField(
        label='body',
        initial='Your comment to opinion. Why you like it?',
        max_length=3000,
        widget=forms.Textarea)


class DisagreeCommentCreationForm(forms.Form):
    body = forms.CharField(
        label='body',
        initial="Your comment to opinion, why you don't agree",
        max_length=3000,
        widget=forms.Textarea)
    linkA = forms.URLField(
        label='linkA', initial='Add link 1', max_length=2000)
    linkB = forms.URLField(
        label='linkB', initial='Add link 2', max_length=2000)


class SearchForm(forms.Form):
    SEARCH_TYPES = (('user', 'Users'), ('post', 'Posts'))
    type = forms.ChoiceField(
        label='search_type', widget=forms.RadioSelect, choices=SEARCH_TYPES)
    query = forms.CharField(label='query', max_length=30)


class GetUserPostsForm(forms.Form):
    user_id = forms.IntegerField(label='user_id')
