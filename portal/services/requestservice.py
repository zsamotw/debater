"""Request service"""

import requests


def get_html_content(url):
    r = requests.get(url)
    return r.text
