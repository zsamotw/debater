"""Date services"""
from django.utils import timezone

MINUTE = 60
HOUR = 3600


def get_period_from_date_to_now(date):
    delta = timezone.now() - date
    if delta.days > 0:
        return '{} days ago'.format(delta.days)
    else:
        seconds = delta.seconds
        if seconds < MINUTE:
            return '{} seconds ago'.format(seconds // MINUTE)
        elif seconds < HOUR:
            return '{} minutes ago'.format(seconds // MINUTE)
        elif seconds >= HOUR:
            return '{} hours ago'.format(seconds // HOUR)
        else:
            "oops something wrong"
