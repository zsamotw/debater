"""PostSearch service"""

from ..models import PostSearch
from ..services import nlpservice


def update(query, user):
    nouns = nlpservice.get_words_with_grammar_criterium(
        query, nlpservice.NOUN_PATTERN)
    for noun in nouns:
        safe_noun = noun.lower()
        try:
            search_result = PostSearch.objects.get(
                word=safe_noun, user__id=user.id)
        except (PostSearch.DoesNotExist):
            initial = PostSearch(word=safe_noun, occurrences=1, user=user)
            initial.save()
            return initial
        else:
            occs = search_result.occurrences + 1
            search_result.occurrences = occs
            search_result.save()
            return search_result


def get_searched_keywords(user):
    searched_keywords = (search.word for search in PostSearch.objects.all()
                      if search.user.id == user.id)
    return searched_keywords
