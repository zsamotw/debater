"""Keyword services"""

from ..models import Keyword
from ..services import nlpservice


def create(word, post):
    safe_word = word.lower()
    word = Keyword.objects.create(word=safe_word, post=post)
    return word


def create_from_post(post):
    nouns = nlpservice.get_words_with_grammar_criterium(
        post.body, nlpservice.NOUN_PATTERN)
    for noun in nouns:
        create(noun, post)


def get_row_words_for_post(post):
    return {
        keyword.word
        for keyword in Keyword.objects.filter(post_id=post.id)
    }
