"""Natural Language Processing service services"""

import re
from concurrent.futures import ThreadPoolExecutor as Executor

from textblob import TextBlob, Word
from textblob.classifiers import NaiveBayesClassifier

from ..models import Classifier

NOUN_PATTERN = re.compile('NN.?')


def lemmatize(word):
    return word.lemmatize().lower()


def text_blob_tags(query):
    text_blob = TextBlob(query)
    return text_blob.tags


def get_words_with_grammar_criterium(query, pattern):
    tex_blob_tags = text_blob_tags(query)
    with Executor(max_workers=3) as exe:
        jobs = (exe.submit(lemmatize, Word(tag[0])) for tag in tex_blob_tags
                if pattern.match(tag[1]))
        results = [job.result() for job in jobs]
        return results


def classify_post_with_categories(post, categories):
    return (Classifier(text=post.body, category=category.category)
            for category in categories)


def classify_post_with_categories_and_save(post, categories):
    classifiers = classify_post_with_categories(post, categories)
    for classifier in classifiers:
        classifier.save()


def get_classifier_with_train_data(data):
    return NaiveBayesClassifier(data)


def run_classifier():
    data = [(cla.text, cla.category) for cla in Classifier.objects.all()]
    print(data)
    return get_classifier_with_train_data(data)


def classify_text(text, classifier):
    return classifier.classify(text)
