"""Post services"""

from django.db.models import Avg, Count, Q
from django.utils import timezone

from ..models import Comment, Post
from ..services import (categoryservice, keywordservice, nlpservice,
                        postsearchservice, usersearchservice)


def create(title, body, creation_date, author, categories):
    post = Post(
        title=title,
        body=body,
        author=author,
        creation_date=creation_date,
        categories=categories)
    post.save()
    word = keywordservice.create_from_post(post)
    return (post, word)


def create_from_form(form, author):
    title = form.cleaned_data['title'].strip()
    body = form.cleaned_data['body'].strip()
    categories_text = form.cleaned_data['categories'].strip()
    creation_date = timezone.now()
    categories = categoryservice.get_categories_from_string_and_save(
        categories_text)
    post = Post(
        title=title, body=body, author=author, creation_date=creation_date)
    post.save()
    categoryservice.add_categories_to_post(post, categories)
    keywordservice.create_from_post(post)
    nlpservice.classify_post_with_categories_and_save(post, categories)


def get_for_user_prefs(user, how_many_users, how_many_posts, how_many_words):
    most_visited_users_id = list(
        usersearchservice.get_searched_users_ids(user))[:how_many_users]
    most_searched_words = list(
        postsearchservice.get_searched_keywords(user))[:how_many_words]
    most_searched_words_as_set = set(most_searched_words)
    post_for_user = (post for post in Post.objects.all()
                     if post.author.id in most_visited_users_id
                     or keywordservice.get_row_words_for_post(post) &
                     most_searched_words_as_set != set())
    return list(post_for_user)[:how_many_posts]


def get_with_comments(posts):
    posts_with_comments = [(post, Comment.objects.filter(post_id=post.id))
                           for post in posts]
    return posts_with_comments


def get_hot_posts(time_filter):
    post_with_comments = Post.objects.annotate(num_comments=Count('comment'))
    avg_num_comments = post_with_comments.aggregate(
        Avg('num_comments'))['num_comments__avg']
    hot_post = post_with_comments.filter(
        time_filter & Q(num_comments__gt=avg_num_comments))
    return hot_post
