"""UserSearch service"""

from ..models import UserSearch


def update(searched_user_id, user):
    try:
        search_result = UserSearch.objects.get(
            searched_user_id=searched_user_id, user=user)
    except (UserSearch.DoesNotExist):
        initial = UserSearch(
            searched_user_id=searched_user_id, occurrences=1, user=user)
        initial.save()
        return initial
    else:
        occs = search_result.occurrences + 1
        search_result.occurrences = occs
        search_result.save()
        return search_result


def get_searched_users_ids(user):
    searched_users_ids = (search.searched_user_id
                          for search in UserSearch.objects.all()
                          if search.user.id == user.id)
    return searched_users_ids
