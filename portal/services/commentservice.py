from django.utils import timezone

from ..models import Comment, Post


def create_agree_comment_from_form(form, author, post_id):
    body = form.cleaned_data['body'].strip()
    post = Post.objects.get(id=post_id)
    creation_date = timezone.now()
    type = 'A'

    comment = Comment(
        body=body,
        author=author,
        post=post,
        creation_date=creation_date,
        type=type)
    comment.save()


def create_disagree_comment_from_form(form, author, post):
    body = form.cleaned_data['body'].strip()
    # post = Post.objects.get(id=post_id)
    creation_date = timezone.now()
    type = 'D'
    linkA = form.cleaned_data['linkA']
    linkB = form.cleaned_data['linkB']
    comment = Comment(
        body=body,
        author=author,
        post=post,
        creation_date=creation_date,
        type=type,
        linkA=linkA,
        linkB=linkB)
    comment.save()
