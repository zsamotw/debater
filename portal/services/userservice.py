"""User service"""

from django.db.models import Q

from ..models import User


def create_from_form(form):
    email = form.cleaned_data['email']
    first_name = form.cleaned_data['first_name'].strip()
    last_name = form.cleaned_data['last_name'].strip()
    password = form.cleaned_data['password']
    if User.objects.filter(Q(username=email) | Q(email=email)):
        raise ValueError('User with that email or username exists')
    user = User.objects.create_user(
        username=email,
        email=email,
        first_name=first_name,
        last_name=last_name,
        password=password)
    return user


def get_from_request(request):
    return request.user
