"""Categories service"""
import re

from ..models import Category


def get_or_create_category(category_name):
    category = Category.objects.filter(category=category_name)
    if not category:
        return Category.objects.create(category=category_name)
    else:
        return category.first


def get_categories_names_from_string(text):
    return [cat.strip() for cat in re.split('\W+', text)]


def get_categories_from_string_and_save(text):
    categories_list = get_categories_names_from_string(text)
    categories = [
        get_or_create_category(category_name)
        for category_name in categories_list
    ]

    return categories


def add_categories_to_post(post, categories):
    for category in categories:
        post.categories.add(category)
