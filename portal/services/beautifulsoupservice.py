"""BeautifulSoup service """

import re

from bs4 import BeautifulSoup
from portal.models import Keyword

from portal.services import requestservice


def get_soup(content):
    soup = BeautifulSoup(content, 'html.parser')
    return soup


def is_keyword_in_soup(keyword, soup):
    word = keyword.word.strip()
    # if word == '':
    #     return False
    # else:
    pattern = re.compile(word, re.IGNORECASE)
    result = soup.find(string=pattern)
    return False if result is None else True


def get_how_many_keywords_in_soup(keywords, soup):
    points = 0
    for keyword in keywords:
        is_in_soup = is_keyword_in_soup(keyword, soup)
        if is_in_soup:
            points += 1
    return points


def is_link_content_connected_with_post(url, post):
    keywords = Keyword.objects.filter(post_id=post.id)
    content = requestservice.get_html_content(url)
    soup = get_soup(content)
    points = get_how_many_keywords_in_soup(keywords, soup)
    result = True if points >= 5 else False
    return result
