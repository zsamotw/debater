from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import redirect, render

from ..forms import LoginForm
from ..models import User
from ..services import postservice
from .forms import (comment_agree_creation_form,
                    comment_disagree_creation_form, login_form,
                    post_creation_form, search_form, user_creation_form)


def index(request):
    context = {
        'login_form': login_form,
        'user_creation_form': user_creation_form
    }
    return render(request, 'portal/index.html', context)


def logIn(request):
    login_form = LoginForm(request.POST)
    if login_form.is_valid():
        user_email = login_form.cleaned_data['user_email']
        user_password = login_form.cleaned_data['user_password']
        user = authenticate(
            request, username=user_email, password=user_password)
        if user is not None:
            login(request, user)
            return redirect('/session/posts/user={}'.format(user.id))
        else:
            return HttpResponse("Wrong password or login. Check it out!!! ")


def logOut(request):
    logout(request)
    return redirect('/')


@login_required
def show_mainboard(request, user_id):
    user = User.get_by_id(user_id)
    num_of_users_to_search = 5
    num_of_posts_to_find = 3
    num_of_words_to_find = 3
    posts = postservice.get_for_user_prefs(user, num_of_users_to_search,
                                           num_of_posts_to_find,
                                           num_of_words_to_find)
    posts_with_comments = postservice.get_with_comments(posts)

    posts_description = 'Posts for you {}'.format(user.first_name)
    context = {
        'search_form': search_form,
        'post_creation_form': post_creation_form,
        'comment_agree_creation_form': comment_agree_creation_form,
        'comment_disagree_creation_form': comment_disagree_creation_form,
        'posts_with_comments': posts_with_comments,
        'user': user,
        'posts_description': posts_description
    }

    return render(request, 'portal/posts.html', context)
