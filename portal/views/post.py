from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.db.models import Q

from django.utils import timezone
import datetime
from ..forms import PostCreationForm
from ..models import Post, User
from ..services import postservice, userservice, usersearchservice
from .forms import (comment_agree_creation_form,
                    comment_disagree_creation_form, post_creation_form,
                    search_form)


@login_required
def create_post(request):
    post_creation_form = PostCreationForm(request.POST)
    if post_creation_form.is_valid():
        author = userservice.get_from_request(request)
        postservice.create_from_form(post_creation_form, author)

        return redirect('/session/posts/user={}'.format(author.id))
    else:
        return HttpResponse('Something wrong with post form')


@login_required
def get_user_posts(request, searched_user_id):
    searched_user = User.get_by_id(searched_user_id)
    user_posts = Post.objects.get_author_posts(searched_user_id)
    posts_with_comments = postservice.get_with_comments(user_posts)
    user = userservice.get_from_request(request)
    usersearchservice.update(searched_user_id, user)
    posts_description = 'User: {} {} posts'.format(searched_user.first_name,
                                                   searched_user.last_name)
    context = {
        'search_form': search_form,
        'post_creation_form': post_creation_form,
        'comment_agree_creation_form': comment_agree_creation_form,
        'comment_disagree_creation_form': comment_disagree_creation_form,
        'posts_with_comments': posts_with_comments,
        'posts_description': posts_description
    }

    return render(request, 'portal/posts.html', context)


@login_required
def get_hot_posts(request):
    seven_days_ago = timezone.now() - datetime.timedelta(days=7)
    week_filter = Q(creation_date__gt=seven_days_ago)
    hot_posts = postservice.get_hot_posts(week_filter)
    posts_with_comments = postservice.get_with_comments(hot_posts)
    posts_description = 'Hot posts from last 7 days'
    context = {
        'search_form': search_form,
        'post_creation_form': post_creation_form,
        'comment_agree_creation_form': comment_agree_creation_form,
        'comment_disagree_creation_form': comment_disagree_creation_form,
        'posts_with_comments': posts_with_comments,
        'posts_description': posts_description
    }

    return render(request, 'portal/posts.html', context)
