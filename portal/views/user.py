from django.shortcuts import redirect
from django.contrib.auth import login
from django.http import HttpResponse

from ..forms import (UserCreationForm)
from ..services import userservice


def create_user(request):
    user_creation_form = UserCreationForm(request.POST)
    if user_creation_form.is_valid():
        try:
            user = userservice.create_from_form(user_creation_form)
        except ValueError:
            return HttpResponse('User with this username or email exists')
        else:
            login(request, user)
            return redirect('/session/posts/user={}'.format(user.id))
