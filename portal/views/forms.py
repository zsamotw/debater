from ..forms import (LoginForm, UserCreationForm, PostCreationForm,
                     CommentCreationForm, DisagreeCommentCreationForm,
                     SearchForm, GetUserPostsForm)
"""
Forms for views
"""
login_form = LoginForm()
user_creation_form = UserCreationForm()
search_form = SearchForm()
post_creation_form = PostCreationForm()
comment_agree_creation_form = CommentCreationForm()
comment_disagree_creation_form = DisagreeCommentCreationForm()
get_user_posts_form = GetUserPostsForm()
