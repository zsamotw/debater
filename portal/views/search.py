from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import redirect, render

from ..forms import SearchForm
from ..models import Post, User
from ..services import postservice, postsearchservice, userservice
from .forms import (comment_agree_creation_form,
                    comment_disagree_creation_form, get_user_posts_form,
                    post_creation_form, search_form)


@login_required
def search(request):
    search_form = SearchForm(request.GET)
    if search_form.is_valid():
        type = search_form.cleaned_data['type']
        query = search_form.cleaned_data['query']
        if type == 'user':
            return redirect('/session/users/{}'.format(query))
        elif type == 'post':
            return redirect('/session/posts/{}'.format(query))
        else:
            return HttpResponse('Opppps no such type of search!!!!!')


@login_required
def search_posts(request, query):
    posts_from_search = Post.objects.look_for(query)
    posts_with_comments = postservice.get_with_comments(posts_from_search)
    user = userservice.get_from_request(request)
    posts_description = 'Posts from query: \"{}\"'.format(query)
    postsearchservice.update(query, user)
    context = {
        'search_form': search_form,
        'post_creation_form': post_creation_form,
        'comment_agree_creation_form': comment_agree_creation_form,
        'comment_disagree_creation_form': comment_disagree_creation_form,
        'posts_with_comments': posts_with_comments,
        'query': query,
        'posts_description': posts_description
    }

    return render(request, 'portal/posts.html', context)


@login_required
def search_users(request, query):
    users_from_search = User.look_for(query)
    context = {
        'get_user_posts_form': get_user_posts_form,
        'users_from_search': users_from_search,
        'query': query
    }
    return render(request, 'portal/users.html', context)
