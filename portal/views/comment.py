from django.shortcuts import redirect

from ..forms import (
    CommentCreationForm,
    DisagreeCommentCreationForm,
)
from ..services import commentservice, userservice
from django.contrib.auth.decorators import login_required
from ..models import Post


@login_required
def create_agree_comment(request, post_id):
    comment_creation_form = CommentCreationForm(request.POST)
    if comment_creation_form.is_valid():
        author = userservice.get_from_request(request)
        commentservice.create_agree_comment_from_form(comment_creation_form,
                                                      author, post_id)

    return redirect('/session/posts/user={}'.format(author.id))


@login_required
def create_disagree_comment(request, post_id):
    comment_creation_form = DisagreeCommentCreationForm(request.POST)
    post = Post.objects.get(id=post_id)
    author = userservice.get_from_request(request)
    if comment_creation_form.is_valid():
        commentservice.create_disagree_comment_from_form(
            comment_creation_form, author, post)

    return redirect('/session/posts/user={}'.format(author.id))
