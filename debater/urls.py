"""debater URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from portal.views.comment import (create_agree_comment,
                                  create_disagree_comment)
from portal.views.post import (create_post, get_user_posts, get_hot_posts)
from portal.views.search import (search, search_posts, search_users)
from portal.views.session import (index, logIn, logOut, show_mainboard)
from portal.views.user import (create_user)


app_name = 'debater'
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('login/', logIn, name='login'),
    path('logout/', logOut, name='logout'),
    path('users/', create_user, name='users_creation'),
    path('session/posts/', create_post, name='post_creation'),
    path('session/posts/user=<int:user_id>', show_mainboard, name='mainboard'),
    path('session/posts/hotposts', get_hot_posts, name='hot_posts'),
    path(
        'session/posts/<int:post_id>/comment/',
        create_agree_comment,
        name='create_agree_comment'),
    path(
        'session/posts/<int:post_id>/disagreecomment/',
        create_disagree_comment,
        name='create_disagree_comment'),
    path('session/search/', search, name='search'),
    path('session/posts/<str:query>/', search_posts, name='search_posts'),
    path('session/users/<str:query>/', search_users, name='search_users'),
    path(
        'session/users/<int:searched_user_id>/posts',
        get_user_posts,
        name='get_user_posts')
]
