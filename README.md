# Debater

Python-based Web application for sharing opinions and conducting discussions based on Django Web Framework.
The main idea of this application is to make users to present proper arguments and prevent from fake pieces of information.

####CHECK OUT develop branch, which is far forward then master.

### Currently it is possible:
* Login.
* add own opinion as post.
* add comments to opinions. If somebody not agree with opinion he has to add two links to web resources as arguments.
* search database looking for other users.
* search database looking for posts.
* application saves data about search queries: users' ids in case of user search and nouns in case of post search.
* application checks user's ids which were searched before and nouns from queries. All be used to find proper posts for user. 
* user could add categories to  post during creating it.
* System classifies posts with categories.

### Model:
* User
* Post
* Comment
* DisagreeComment
* Search: UserSearch, PostSearch
* Keyword
* Category
* Classifier

### Technologies:
* Python
* Django
* TextBlob
* Factory Boy
